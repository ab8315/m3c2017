"""Final project, part 1"""
import numpy as np
import matplotlib.pyplot as plt
from p2 import part2 #assumes p2.f90 has been compiled with f2py into module, p2.xxx.so



def analyze1():
    """Analyze behavior of Psi(t) for no-noise pedestrian model
        Add input/output variables as needed
    """


    return None

def analyze2():
    """Analyze influence of noise on Psi
        Add input/output variables as needed
    """


    return None

def performance():
    """Analyze performance of simulate_omp
    Add input/output variables as needed
    """


    return None



if __name__=='__main__':
    #Modify input/output as needed to generate final figures with call(s) below
    input=()
